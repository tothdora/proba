package comm;

public class Vehicle {
    protected String type;
    protected Integer age;

    public Vehicle(String tipus, Integer kor){
        this.type=tipus;
        this.age=kor;
    }
    public String getType(){
        return this.type;
    }
    public void setType(String tipus){
        this.type=tipus;
    }
    public Integer getAge(){
        return this.age;
    }

    public void setAge(Integer kor){
        this.age=kor;
    }
}
