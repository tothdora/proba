package comm;

public class Car extends Vehicle{
    private float performance;

    public Car(String tipus, Integer kor, Float perf){
        super(tipus,kor);
        this.performance=perf;
    }

    public float getPerformance(){
        return this.performance;
    }

    public void setPerformance(float perf){
        this.performance=perf;
    }

    @Override
    public String toString(){
        return getType() + " " + getAge() + " " + this.performance;
    }
}
