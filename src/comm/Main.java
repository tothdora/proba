package comm;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");
        Car Dacia = new Car("duster",5, (float) 12.2);
        Car Mercedes = new Car("G500",6, (float) 20.2);
        System.out.println(Dacia.toString());
        CarList carList = new CarList(2);
        carList.addCar(Dacia);
        carList.addCar(Mercedes);

        CarList.CarIterator i = carList.getIterator();
        while(i.hasMoreElements()==true)
        {
            System.out.println(i.nextElement());

        }
    }
}
