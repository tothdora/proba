package comm;

public class CarList {
    private static Car[] cars;
    private Integer current = 0;

    public CarList(int size) {
        cars = new Car[size];
    }

    public void addCar(Car autok) {
        if (current < cars.length) {
            cars[current] = autok;
            current = current + 1;
        } else
            System.out.println("nincs tobb hely");

    }

    public CarIterator getIterator() {
        return new CarIterator();
    }


    public class CarIterator {
        private int index;

        public CarIterator() {
            index = 0;
        }

        public boolean hasMoreElements() {
            if (index < cars.length)
                return true;
            return false;
        }

        public Car nextElement() {
            index = index + 1;
            return cars[index-1];
        }
    }
}
